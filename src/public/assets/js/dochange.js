$(function(){
    function overlayswap() {
        $('.singleartwork').hide();
        var overlaychosen = $('#whichoverlay').find(':selected').data('overlay');
        //console.log('overlaychosen: ' + overlaychosen );
        if (overlaychosen === 'none') {
            $('#fullsizeoverlaypreview').css('background-image', 'none');
            //console.log('removed fullsizeoverlaypreview.');
        } else {
            $('#fullsizeoverlaypreview').css('background-image', 'url("/assets/artwork/' + overlaychosen + '")');
            //console.log('added overlaygraphic: /assets/artwork/' + overlaychosen);
        }
    }
    $(document).on('change', '#whichoverlay', function(){
        overlayswap();
    });
    $(document).on('click', '#deleteit', function(e){
        e.preventDefault();
        var del = confirm('Are you sure you want to throw away this artwork?');
        if (del == true)
        {
            $('#deleteform').submit();
        }
    });

    $(document).ready(function(){
        sizeSingleWrapper();
    });
    $(window).on('resize', function(){
        sizeSingleWrapper();
    });

    function sizeSingleWrapper()
    {
        var wide = 512;
        if ($('body').width() < wide) {
            var wide = $('body').width();
        }
        $('.singlewrapper, #fullsizeoverlaypreview').css('width', wide).css('height', wide);
    }

    $('#changedate').datetimepicker();

    var userNameTypeahead = new Bloodhound({
        datumTokenizer: function (d) {
            return Bloodhound.tokenizers.whitespace(d.value);
        },
        queryTokenizer: Bloodhound.tokenizers.whitespace,
        remote: {
            url: '/userspartial/%QUERY',
            filter: function ( response ) {
                return $.map(response.users, function (object) {
                    return {
                        value: object.username,
                        username: object.username,
                        userID: object.id
                    };
                });
            }
        }
      });
      userNameTypeahead.initialize();

      //instantiate the typeahead UI for User Name Lookup
      $('#changeartist').typeahead(null, {
        minLength: 1,
        displayKey: 'value',
        source: userNameTypeahead.ttAdapter()
      }).on('typeahead:selected', function (object, datum) { 
        $('#changeartist').val(datum.username); 
        $('#changeartistid').val(datum.userID);
      });
});
