<?php

class Episode extends Eloquent
{
    protected $fillable = array('episode_number', 'show_date', 'link', 'title');

    public function artwork()
    {
        return $this->hasMany('Artwork');
    }

    public function albumart($artwork_id)
    {
        $artwork = Artwork::find($artwork_id);
        return $artwork;
    }

}
