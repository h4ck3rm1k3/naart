<?php

class ArtworkController extends BaseController
{

    /**
     * Artwork Model
     * @var Artwork
     */
    protected $artwork;

    /**
     * User Model
     * @var $user
     */
    protected $user;

    /**
     * Inject the models
     * @param Artwork $artwork
     * @parm User $user
     */
    public function __construct(Artwork $artwork, User $user)
    {
        parent::__construct();
        $this->artwork = $artwork;
        $this->user = $user;
    }

    /**
     * Displays submitted artwork
     *
     * @return Response
     */
    public function index()
    {
        $artworks = Artwork::whereNotNull('artworks.episode_id')->orderBy('artworks.episode_id', 'DESC')->orderBy('artworks.created_at', 'DESC')->paginate(52);
        $checkartworks = array();
        foreach ($artworks as $artwork) {
            $checkartworks[] = $artwork->id;
        }
        $episodes = Episode::whereRaw('artwork_id IN (' . implode(',', $checkartworks) .')')->get();
        foreach($episodes as $episode)
        {
            foreach ($artworks as $artwork)
            {
                if ($artwork->id == $episode->artwork_id) {
                    $artwork->accepted_for = $episode;
                }
            }   
        }
        return View::make('gallery', compact('artworks'));
    }

    public function episodeIndex()
    {
        $user = Auth::user();
        if (!is_null($user) && $user->can('manage_episodes')) {
            $episodes = Episode::orderBy('episodes.episode_number', 'DESC')->paginate(52);
        } else {
            $episodes = Episode::where('link', '!=', '')->orderBy('episodes.episode_number', 'DESC')->paginate(52);
        }
        
        $title = 'Accepted Artwork';
        return View::make('episodes', compact('episodes', 'title'));
    }

    /**
     * Displays Evergreen Artworks
     *
     * @return Response
     */
    public function evergreens()
    {
        $artworks = Artwork::whereNull('artworks.episode_id')->orderBy('artworks.created_at', 'DESC')->paginate(52);
        $checkartworks = array();
        foreach ($artworks as $artwork) {
            $checkartworks[] = $artwork->id;
        }
        $episodes = Episode::whereRaw('artwork_id IN (' . implode(',', $checkartworks) .')')->get();
        foreach($episodes as $episode)
        {
            foreach ($artworks as $artwork)
            {
                if ($artwork->id == $episode->artwork_id) {
                    $artwork->accepted_for = $episode;
                }
            }   
        }
        return View::make('gallery', compact('artworks'));
    }

    /**
     * Displays a single artwork submission
     *
     * @return Response
     */
    public function view($artworkid)
    {
        $artwork = Artwork::findOrFail($artworkid);
        $user = Auth::user();
        $can_edit = false;
        $can_change_details = false;
        $overlays = null;
        if (!is_null($user)) {
            if ($user->can('manage_artwork') || ($artwork->user_id == $user->id && Carbon::now()->lt(Carbon::createFromTimestamp($artwork->created_at)->addMinutes(15)))) {
                $can_edit = true;
                $overlays = Overlay::all();
                if ($user->can('manage_artwork'))
                {
                    $can_change_details = true;
                }
            }
        }
        $title = $artwork->title;
        return View::make('single',
            compact('title', 'artwork', 'user', 'can_edit', 'can_change_details', 'overlays'));
    }

    public function artistProfile($artistid)
    {
        $artist = User::findOrFail($artistid);
        $user = Auth::user();
        $can_edit = false;
        if (!is_null($user) && ($user->can('manage_users') || $artist->id == $user->id)) {
            $can_edit = true;
        }
        if (!is_null($artist))
        {
            $title = $artist->profile->name;
            $artworks = Artwork::where('user_id', '=', $artist->id)->orderBy('created_at', 'DESC')->paginate(48);
            $checkartworks = array();
            foreach ($artworks as $artwork) {
                $checkartworks[] = $artwork->id;
            }
            if (count($artworks) > 0) {
                $episodes = Episode::whereRaw('artwork_id IN (' . implode(',', $checkartworks) .')')->get();
                foreach($episodes as $episode)
                {
                    foreach ($artworks as $artwork)
                    {
                        if ($artwork->id == $episode->artwork_id) {
                            $artwork->accepted_for = $episode;
                        }
                    }   
                }
            }
        }
        return View::make('profile',
            compact('title', 'artworks', 'artist', 'can_edit'));
    }

    public function viewEpisode($episodeid)
    {
        $episode = Episode::findOrFail($episodeid);
        $user = Auth::user();
        $can_edit = false;
        if (!is_null($user)) {
            if ($user->can('manage_artwork')) {
                $can_edit = true;
            }
        }
        $title = $episode->title;
        $approved_art = Artwork::find($episode->artwork_id);
        $submitted_art = Artwork::where('episode_id', '=', $episode->id)->orderBy('created_at', 'DESC')->paginate(18);
        $show_date = Carbon::createFromFormat('Y-m-d', $episode->show_date);
        $showdate  = $show_date->format('l, F j');
        $showdate .= '<sup>' . $show_date->format('S') . '</sup>, ';
        $showdate .= $show_date->format('Y');
        return View::make('singleepisode', compact('title', 'episode', 'user', 'can_edit', 'showdate', 'approved_art', 'submitted_art'));
    }

    public function edit()
    {
        $rules = array(
            'artworkid'    => 'required',
            'whichoverlay' => 'required',
            'title'        => 'required|min:3',
            'changeartistid' => 'numeric',
            'changedate'     => 'date',
        );
        $validator = Validator::make(Input::all(), $rules);
        $artwork = Artwork::findOrFail(Input::get('artworkid'));
        if ($validator->passes()) {
            if (!is_null(Input::get('changeartistid')))
            {
                $artworkartist = User::find(Input::get('changeartistid'));
                if (is_null($artworkartist))
                {
                    return Redirect::to('/artwork/' . $artwork->id)->withInput()->with('errors', Lang::get('branding.artist.doesntexist'));
                }
                $newcreated = Carbon::createFromFormat('Y-m-d h:i A', Input::get('changedate'))->format('Y-m-d H:i:s');
                $artwork->user_id = $artworkartist->id;
                $artwork->created_at = $newcreated;
            }
            $artwork->title = Input::get('title');
            $artwork->save();
            $user = Auth::user();
            if (!is_null($user)) {
                if ($user->can('manage_artwork') || ($artwork->user_id == $user->id && Carbon::now()->lt(Carbon::createFromTimestamp($artwork->created_at)->addMinutes(15)))) {
                    if (Input::get('whichoverlay') == 'none') {
                        $whichoverlay = null;
                    } else {
                        $whichoverlay = Input::get('whichoverlay');
                    }
                    self::replaceOverlay($artwork, $whichoverlay);
                    return Redirect::to('/artwork/' . $artwork->id)->with('success', Lang::get('branding.artwork.updated'));
                } else {
                    return Redirect::to('/artwork/' . $artwork->id)->withInput()->with('errors', Lang::get('branding.artwork.cannotupdate'));
                }
            }
        }
        return Redirect::to('/artwork/' . $artwork->id)->withInput()->withErrors($validator);
    }

    public function delete() {
        $rules = array(
            'artworkid' => 'required'
        );
        $validator = Validator::make(Input::all(), $rules);
        if ($validator->passes())
        {
            $artwork = Artwork::findOrFail(Input::get('artworkid'));
            $user = Auth::user();
            if (!is_null($user)) {
                if ($user->can('manage_artwork') || ($artwork->user_id == $user->id && Carbon::now()->lt(Carbon::createFromTimestamp($artwork->created_at)->addMinutes(15)))) {
                    $artwork->delete();
                    return Redirect::to('/artworks')->with('success', Lang::get('branding.artwork.artdeleted'));
                }
            }
        }
        return Redirect::to('/artwork/' . $artwork->id)->withErrors($validator);
    }

    public function updateEpisode()
    {
        $rules = array(
            'episodeid'       => 'required|numeric',
            'episodenumber'   => 'required|numeric',
            'acceptedartwork' => 'required|numeric',
            'showtitle'       => 'required',
            'showdate'        => 'required|date',
            'showlink'        => 'required|url',
            'published'       => 'required|boolean',
        );
        $validator = Validator::make(Input::all(), $rules);
        if ($validator->passes())
        {
            $episode = Episode::findOrFail(Input::get('episodeid'));
            $episode->title          = Input::get('showtitle');
            $episode->show_date      = Input::get('showdate');
            $episode->artwork_id     = Input::get('acceptedartwork');
            $episode->episode_number = Input::get('episodenumber');
            $episode->link           = Input::get('showlink');
            $episode->published      = Input::get('published');
            $episode->save();
            return Redirect::to('/episode/' . $episode->id)->with('Success', Lang::get('branding.episode.updated'));
        }
        return Redirect::to('/episode/' . Input::get('episodeid'))->withErrors($validator);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return Response
     */
    public function create()
    {
        $title = Lang::get('branding.artwork.create_title');
        return View::make('createartwork', compact('title'));
    }

    /**
     * Present the cropping interface for a newly uploaded image
     *
     * @return Response
     */
    public function crop()
    {
        $rules = array(
            'newimagepath' => 'required',
        );
        $validator = Validator::make(Input::all(), $rules);
        if (file_exists(public_path() . Input::get('newimagepath')) && $validator->passes()) {
            $title = 'Crop It!';
            $overlays = Overlay::where('approved', true)->get();
            $artworkname = Input::get('newimagename');
            $artwork = Input::get('newimagepath');
            return View::make('cropartwork', compact('title', 'artwork', 'artworkname', 'overlays'));
        }
        return Redirect::to('/create')->withInput()->withErrors($validator);
    }

    /**
     * Stash an uploaded image in storage.
     *
     * @return Response
     */
    public function stash()
    {
        $rules = array(
            'artworkfile' => 'required|image|max:10240',
        );
        $validator = Validator::make(Input::all(), $rules);
        if ($validator->passes()) {
            if (Input::hasFile('artworkfile')) {

                $file = Input::file('artworkfile');
                $newartworkid = time();
                $ext = $file->getClientOriginalExtension();
                $newartworkname = Hashids::encode($newartworkid, Auth::user()->id) . '.' . $ext;
                $carbon = Carbon::now();
                $month = $carbon->format('m');
                $year = $carbon->format('Y');
                $yearpath = public_path() . '/assets/artwork/uncropped/' . $year;
                $monthpath = public_path() . '/assets/artwork/uncropped/' . $year . '/' . $month;
                if (!file_exists($yearpath)) {
                    if (!mkdir($yearpath, 0777, true)) {
                        $response = array(
                            'success' => false,
                            'errors'  => array(array('message' => 'Could not create year path: ' . $yearpath)),
                        );
                        return Response::json($response);
                    }
                }
                if (!file_exists($monthpath)) {
                    if (!mkdir($monthpath, 0777, true)) {
                        $response = array(
                            'success' => false,
                            'errors'  => array(array('message' => 'Could not create month path: ' . $monthpath)),
                        );
                        return Response::json($response);
                    }
                }
                $file->move($monthpath . '/', $newartworkname);
                $response = array(
                    'success'      => true,
                    'newimagepath' => '/assets/artwork/uncropped/' . $year . '/' . $month . '/' . $newartworkname,
                    'newimagename' => $newartworkname,
                );
                return Response::json($response);
            }
            return Response::json(array('success' => false, 'message' => 'Did not have file'));
        }
        $errors = $validator->getMessageBag()->toArray();
        $response = array(
            'success' => false,
            'errors'  => $errors,

        );
        return Response::json($response);
    }

    /**
     * Finalizes and stores the cropped artwork with overlay.
     *
     * @return Response
     */
    public function process()
    {
        $rules = array(
            'img_src'      => 'required',
            'img_data'     => 'required',
            'whichoverlay' => 'required',
            'whichepisode' => 'required',
            'artworktitle' => 'required|min:3',
        );
        $nextepisode = Episode::where('published', false)->orderBy('id', 'desc')->first();
        if (is_null($nextepisode)) {
            $currentepisode = Episode::where('published', true)->orderBy('id', 'desc')->first();
            $nextepisode = new Episode();
            $nextepisode->published = false;
            $nextepisode->title = 'TBA';
            $nextepisode->episode_number = $currentepisode->episode_number + 1;
            if (Carbon::createFromFormat('Y-m-d', $currentepisode->show_date)->format('l') === 'Thursday') {
                $nextshow_date = new Carbon('next Sunday');
            } else {
                $nextshow_date = new Carbon('next Thursday');
            }
            $nextepisode->show_date = $nextshow_date->format('Y-m-d');
            $nextepisode->link = '#';
            $nextepisode->save();
        }
        $validator = Validator::make(Input::all(), $rules);
        if ($validator->passes() && file_exists(public_path() . Input::get('img_src'))) {
            if (is_null(Input::get('whichepisode')) || Input::get('whichepisode') == 'evergreen') {
                $whichepisode = 'evergreen';
            } else {
                $whichepisode = $nextepisode->episode_number;
            }
            $img_data = json_decode(stripslashes(Input::get('img_data')));
            $orig_img = Input::get('img_src');
            $artwork_id = self::createAlbumArtwork(
                $img_data, $orig_img, Input::get('whichoverlay'),
                $whichepisode, Auth::user()->id, Input::get('artworktitle'), Carbon::now()->format('Ym'));
            if ($artwork_id) {
                return Redirect::to('artwork/' . $artwork_id)->with('success', Lang::get('branding.artwork.success'));
            }
        }
        return Redirect::route('crop', array(
                'newimagepath' => Input::get('img_src'),
                'newimagename' => basename(public_path() . Input::get('img_src'))
            )
        )->withInput()->withErrors($validator);
    }

    /**
     * @param $artwork
     * @param $newoverlay
     *
     * @return Response
     */
    public static function replaceOverlay($artwork, $newoverlay)
    {
        $cropdata = new stdClass();
        $cropdata->x = 0;
        $cropdata->y = 0;
        $cropdata->width = 512;
        $cropdata->height = 512;
        if (is_null($artwork->episode_id)) {
            $episode = 'evergreen';
        } else {
            $episode = $artwork->episode->episode_number;
        }
        //die($artwork->path . '/crop_' . $artwork->filename . '<br>' . $newoverlay . '<br>' . $artwork->episode->episode_number);
        self::createAlbumArtwork(
            $cropdata,
            $artwork->path . '/crop_' . $artwork->filename,
            $newoverlay,
            $episode,
            $artwork->user_id,
            $artwork->title,
            Carbon::createFromFormat('Y-m-d H:i:s', $artwork->created_at)->format('Ym'),
            true
        );
        return $artwork;
    }

    /**
     * @param $img_data
     * @param $img_src
     * @param $whichoverlay
     * @param $whichepisode
     * @param $artist_user_id
     * @param $artworktitle
     * @param null $created
     * @return int newly created artwork id
     */
    public static function createAlbumArtwork($img_data, $img_src, $whichoverlay, $whichepisode, $artist_user_id, $artworktitle, $created = null, $precropped = false)
    {
        $orig_img = public_path() . $img_src;
        $image_type = exif_imagetype($orig_img);
        switch ($image_type) {
            case IMAGETYPE_GIF:
                $src_img = imagecreatefromgif($orig_img);
                break;
            case IMAGETYPE_JPEG:
                $src_img = imagecreatefromjpeg($orig_img);
                break;
            case IMAGETYPE_PNG:
                $src_img = imagecreatefrompng($orig_img);
                break;
        }
        $dst_img = imagecreatetruecolor(512, 512);
        if ($precropped) {
            $orig_img = str_replace('/crop_', '/', $orig_img);
        }
        imagecopyresampled($dst_img, $src_img,
            0, 0, $img_data->x, $img_data->y, 512, 512,
            $img_data->width, $img_data->height);

        if ($whichepisode == 'evergreen') {
            if (is_null($created)) {
                $artfolder = '/assets/artwork/evergreen/' . Carbon::now()->format('Ym');
            } else {
                $artfolder = '/assets/artwork/evergreen/' . $created;
            }
            $evergreen = true;
        } else {
            $artfolder = '/assets/artwork/episode/' . ($whichepisode + 0);
            $evergreen = false;
        }
        $artworkpath = public_path() . $artfolder;
        if (!file_exists($artworkpath)) {
            if (!mkdir($artworkpath, 0777, true)) {
                // TODO: Warn of error making directory.
                print "Couldn't make directory.\n";
            }
        }
        $info = pathinfo($orig_img);
        $orig_name = basename($orig_img, '.' . $info['extension']);
        if (!$precropped) {
            imagepng($dst_img, $artworkpath . '/crop_' . $orig_name . '.png');
        }
        imagedestroy($src_img);
        if (is_null($whichoverlay) || $whichoverlay == 'none') {
            $overlaysrc = null;
        } else {
            $overlaysrc = Overlay::where('id', $whichoverlay)->first();
            self::overlayArtwork($whichoverlay, $dst_img);
        }
        imagepng($dst_img, $artworkpath . '/' . $orig_name . '.png');
        $thumbs = array(
            'threetwenty'    => 320,
            'twofiftysix'    => 256,
            'oneninetysix'   => 196,
            'onefifty'       => 150,
            'onetwentyeight' => 128,
            'ninetysix'      => 96,
            'seventytwo'     => 72,
        );
        $thumbspath = $artworkpath . '/' . $orig_name . '_thumbs';
        if (!file_exists($thumbspath)) {
            if (!mkdir($thumbspath, 0777, true)) {
                // TODO: Warn of error making directory
                print "Couldn't make artwork thumbs directory.\n";
            }
        }
        foreach ($thumbs as $name => $size) {
            self::createThumbnail($dst_img, $orig_name, 512, $size, $thumbspath);
        }
        imagedestroy($dst_img);
        if (!$precropped) {
            $artwork = new Artwork;
            $artwork->user_id = $artist_user_id;
            $artwork->title = trim($artworktitle);

        } else {
            $artwork = Artwork::where('filehash', '=', $orig_name)->first();
        }
        $artwork->overlay_id = is_null($overlaysrc) ? null : $overlaysrc->id;
        $artwork->filename = $orig_name . '.png';
        $artwork->path = $artfolder;
        $artwork->filehash = $orig_name;
        if (!$evergreen) {
            $nextepisode = Episode::where('episode_number', '=', $whichepisode)->first();
            $artwork->episode_id = $nextepisode->id;
        } else {
            $artwork->episode_id = null;
        }
        if ($artwork->save()) {
            return $artwork->id;
        }

    }

    /**
     * Creates an image overlayed with the selected image after being cropped.
     *
     */
    private static function overlayArtwork($overlay, &$artwork)
    {
        $overlaysrc = Overlay::where('id', $overlay)->first();
        $overlay = public_path() . '/assets/artwork/' . $overlaysrc->filename;
        $frame = imagecreatefrompng($overlay);
        imagealphablending($frame, true);
        imagealphablending($artwork, true);
        self::imagecopymerge_alpha($artwork, $frame, 0, 0, 0, 0, 512, 512, 100);
        imagealphablending($artwork, false);
        imagesavealpha($artwork, true);
        imagedestroy($frame);
    }

    /**
     *  Creates a square thumbnail of an image
     *
     * @return Boolean
     */
    private static function createThumbnail($orignial_image, $original_name, $original_size, $thumbnail_size, $thumbnail_path)
    {
        $thumb = imagecreatetruecolor($thumbnail_size, $thumbnail_size);
        imagecopyresampled($thumb, $orignial_image, 0, 0, 0, 0, $thumbnail_size, $thumbnail_size, $original_size, $original_size);
        if (imagepng($thumb, $thumbnail_path . '/' . $original_name . '_' . $thumbnail_size . '.png')) {
            imagedestroy($thumb);
            return true;
        }
        return false;
    }

    /**
     * PNG ALPHA CHANNEL SUPPORT for imagecopymerge();
     * by Sina Salek
     *
     * Bugfix by Ralph Voigt (bug which causes it
     * to work only for $src_x = $src_y = 0.
     * Also, inverting opacity is not necessary.)
     * 08-JAN-2011
     *
     **/
    private static function imagecopymerge_alpha($dst_im, $src_im, $dst_x, $dst_y, $src_x, $src_y, $src_w, $src_h, $pct)
    {
        // creating a cut resource
        $cut = imagecreatetruecolor($src_w, $src_h);

        // copying relevant section from background to the cut resource
        imagecopy($cut, $dst_im, 0, 0, $dst_x, $dst_y, $src_w, $src_h);

        // copying relevant section from watermark to the cut resource
        imagecopy($cut, $src_im, 0, 0, $src_x, $src_y, $src_w, $src_h);

        // insert cut resource to destination image
        imagecopymerge($dst_im, $cut, $dst_x, $dst_y, 0, 0, $src_w, $src_h, $pct);
    }
}
