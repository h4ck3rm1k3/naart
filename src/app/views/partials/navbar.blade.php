<header id="mainHeader" class="navbar-fixed-top" role="banner">
    <nav class="navbar navbar-default scrollMenu" role="navigation">
        <div class="navbar-header">
            <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target=".navbar-collapse">
                <span class="sr-only">Toggle navigation</span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
            </button>
            <a class="na-img-logo navbar-brand" href="/">{{{ Lang::get('branding.sitename') }}}</a>
        </div>
        <div class="collapse navbar-collapse" id="scrollTarget">
            <ul class="nav navbar-nav">
                @if (Auth::user())
                <li class="{{ Request::segment(1) =='create' ? 'active' : '' }}"><a href="{{ url('create') }}">Add Artwork</a></li>
                @endif
                <li class="{{ Request::segment(1) == 'artworks' ? 'active' : '' }}"><a href="{{ url('artworks') }}">Submitted Art</a>
                <li class="{{ Request::segment(1) == 'evergreen' ? 'active' : '' }}"><a href="{{ url('evergreen') }}">Evergreen Art</a></li>
                <li class="{{ Request::segment(1) == 'episodes' ? 'active' : '' }}"><a href="{{ url('episodes') }}">Accepted Art</a></li>
                @if (Auth::user())
                    <li><a href="{{ url('logout') }}">Logout</a></li>
                @else
                <li class="{{ Request::segment(1) == 'signup' ? 'active' : '' }}"><a href="{{ url('signup') }}">Sign In/Sign Up</a></li>
                @endif
            </ul>
        </div>
    </nav>
</header>
