@extends('layouts.master')

@section('title')
@parent
 :: {{ $title }}
@stop

@section('css')
@parent
<link rel="stylesheet" href="/assets/css/cropper.css">
@stop

@section('content')
<div class="container eg-container" id="basic-example">
	<div class="row">
		<div class="col-md-9 eg-main">
			<h3>Crop Your Image</h3>
			<div class="eg-wrapper">
				  <img class="cropper" src="{{ $artwork }}" alt="Picture">
			</div>
		</div>
		<div class="col-md-3">
			<h3>Preview Image</h3>
		    <div class="eg-preview clearfix" id="preview-wrapper">
				<div id="overlaygraphic"></div>
				<div class="preview preview-lg"></div>
		    </div>
		    <form role="form" id="chooseoverlay" method="post" action="/process">
		    	<input type="hidden" name="_token" value="{{ csrf_token() }}">
		    	<div class="form-group">
		    	    <label for="artworktitle">Give it a title:</label>
		    	    <input type="text" class="form-control" placeholder="Your Title Here"
		    	        maxlength="100" name="artworktitle" value="{{{ Input::old('artworktitle') }}}">
		    	</div>
		    	<div class="form-group">
			    	<label for="whichoverlay">Which Overlay?</label>
			    	<select name="whichoverlay" id="whichoverlay" class="form-control">
					    <option value="none"
					    	data-overlay="none" selected
					    	>No Overlay</option>
					    @foreach ($overlays as $overlay)
					        <option value="{{ $overlay->id }}"
					            data-overlay="preview_{{ $overlay->filename }}">{{ $overlay->title }}</option>
					    @endforeach
					</select>
		    	</div>
		    	<div class="form-group">
		    	    <label for="whichepisode">Episode or Evergreen?</label>
		    	    <select name="whichepisode" class="form-control">
		    	        <option value="next" selected>Next Episode</option>
		    	        <option value="evergreen">Evergreen</option>
		    	    </select>
		    	</div>
		    	<input id="img-src" name="img_src" type="hidden" value="{{ $artwork }}">
				<input id="img-data" name="img_data" type="hidden">
				<div class="form-group">
					<button type="submit"
						class="btn btn-primary btn-large pull-right">Save It!</button>
				</div>
		    </form>
		</div>
	</div>
</div>
@stop

@section('scripts')
@parent
<script src="/assets/js/docrop.js"></script>
@stop
