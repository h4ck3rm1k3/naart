@extends('layouts.master')

@section('title')
@parent
 :: {{ $title }}
@stop

@section('css')
@parent
<link rel="stylesheet" href="/assets/css/cropper.css">
@stop

@section('content')
<div class="container">
    <div class="row center">
        <h1 class="artworktitle">@if (is_null($artwork->episode_id))
            Evergreen
        @else
            Episode {{ $artwork->episode->episode_number + 0 }}
        @endif Artwork<br>&quot;{{ $artwork->title }}&quot; by <a href="/artist/{{ $artwork->user_id }}">{{{ $artwork->user->profile->name }}}</a></h1>
        <div class="singlewrapper"
        @if ($can_edit)
        style="background-image:url('{{ $artwork->path }}/crop_{{ $artwork->filename }}');">
        <div id="fullsizeoverlaypreview"></div>
        @else
        >
        @endif
        <img class="artwork singleartwork" src="{{ $artwork->path }}/{{ $artwork->filename }}" alt="No Agenda Album Art by {{{
        $artwork->user->username
        }}}">
        </div>
        @if ($can_edit)
            <div class="container">
            <div class="row">
                @if ($can_change_details)
                <h3>Change Details, Title and/or Overlay</h3>
                @else
                <h3>Change Title and/or Overlay</h3>
                @endif
                <form role="form" method="post" action="/editartwork" class="form-horizontal">
                    <input type="hidden" name="_token" value="{{{ Session::getToken() }}}">
                    <input type="hidden" name="artworkid" value="{{ $artwork->id }}">
                    @if ($can_change_details)
                        <div class="form-group col-sm-6">
                            <label for="changeartist" class="col-sm-4 control-label">Artist:</label>
                            <div class="col-sm-8">
                                <input type="text" id="changeartist" class="form-control" name="changeartist"
                                value="{{ Input::old('changeartist', isset($post) ? $post->changeartist : $artwork->user->username )}}">
                                <input id="changeartistid" name="changeartistid" 
                                type="hidden"
                                value="{{ Input::old('changeartistid', isset($post) ? $post->changeartistid : $artwork->user->id) }}"> 
                            </div>
                        </div>
                        <div class="form-group col-sm-6">
                            <label for="changedate" class="col-sm-4 control-label">Submitted:</label>
                            <div class="col-sm-8">
                                <input type="text" id="changedate" class="form-control" name="changedate" 
                                data-date-format="YYYY-MM-DD hh:mm A" value="{{ Input::old('changedate', isset($post) ? $post->changedate : $artwork->created_at->format('Y-m-d H:i A')) }}">
                            </div>
                        </div>
                    @endif
                    <div class="form-group col-sm-6">
                        <label for="title" class="col-sm-4 control-label">Change Title:</label>
                        <div class="col-sm-8">
                            <input type="text" name="title" class="form-control" value="{{ Input::old('title', isset($post) ? $post->title : $artwork->title) }}">
                        </div>
                    </div>
                    <div class="form-group col-sm-6">
                        <label for="whichoverlay" class="col-sm-4 control-label">Which Title or Overlay?</label>
                        <div class="col-sm-8">
                            <select name="whichoverlay" id="whichoverlay" class="form-control">
                                @if (is_null($artwork->overlay_id))
                                <option value="none" data-overlay="none" selected>No Overlay</option>
                                @else
                                <option value="none" data-overlay="none">No Overlay</option>
                                @endif
                                @foreach ($overlays as $overlay)
                                    <option value="{{ $overlay->id }}"
                                    @if ($artwork->overlay_id == $overlay->id)
                                        selected
                                    @endif
                                        data-overlay="{{ $overlay->filename }}">{{ $overlay->title }}</option>
                                @endforeach
                            </select>
                        </div>
                    </div>
                    <div class="col-sm-6 col-sm-offset-6">
                        <button type="submit" class="btn btn-primary btn-lg"><span class="fa fa-pencil"></span> Edit Artwork</button>
                    </div>
                </form>
            </div>
            <div class="row deleteart">
            <div class="col-sm-4 col-sm-offset-3">
                <h3 class="pull-right">Or Would You Prefer to Just Delete It?</h3>
            </div>
            <div class="col-sm-2">
                <form class="form-inline" role="form" method="post" id="deleteform" action="/deleteartwork">
                    <input type="hidden" name="_token" value="{{ Session::getToken() }}">
                    <input type="hidden" name="artworkid" value="{{ $artwork->id }}">
                    <button type="submit" id="deleteit" class="btn btn-danger btn-sm pull-left"><span class="fa fa-trash"></span> Delete Artwork</button>
                </form>
            </div>
            </div>
            </div>
        @endif
    </div>
</div>
@stop

@if ($can_edit)
    @section('scripts')
    @parent
    <script src="/assets/js/dochange.js"></script>
    @stop
@endif
