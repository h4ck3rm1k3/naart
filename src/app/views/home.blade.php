@extends('layouts.master')

@section('title')@parent:: Home @stop

@section('content')
<section id="welcome" data-type="background" data-speed="3">
    <div class="container dark-gradient gradient">
        <div class="row">
            <div class="col-xs-3 mastheadimg"><img src="/assets/icon/mstile-150x150.png" alt="No Agenda Art Generator"></div>
            <div class="col-xs-8 col-xs-offset-1 welcometxt">
                <h1>Welcome to the No Agenda Art Generator</h1>
                <h3>Making No Agenda Album Art Better Since 2010</h3>
            </div>
        </div>
    </div>
</section>
<section id="about">
    <div class="container">
        <h2>Welcome to the new and improved No Agenda Art Generator</h2>
        <p>Every effort has been made to make the transition to this newly renovated and recoded from the ground up version of the
        <a href="http://www.noagendashow.com">Best Podcast In The Universe</a>'s Album Art Generator, but you can expect some rough
        edges and dust as this is the first full day in the wild.  If you used the old site, your account has been transferred along
        with all submitted artwork. However, you will need to reset your password because those didn't make the trip over to the modern era
        (they were all salted and hashed hence the reason the migration requires you to create a new one) - you can do that on the 
        <a href="/signup">Login/Signup Page</a>.</p>
        <p>I hope that you find the new digs more enjoyable and easier to use. We scrapped some completely unused parts, and are adding
        some new stuff to make things even better (for example, soon, you'll be able to add overlays to the mix.)</p>
        <p>Here's to several more years punching people in the mouth with pretty pictures.  By the way, for the time being, you can still
        access the old site at <a href="http://old.noagendaartgenerator.com">old.NoAgendaArtGenerator.com</a>.</p>
        <h3>About this site</h3>
        <p>The purpose of the Album Art Generator is to help producers of the <a href="http://www.noagendashow.com">No Agenda Show</a>
        submit and create album cover art for <a href="http://en.wikipedia.org/wiki/Adam_Curry">Adam</a> and
        <a href="http://en.wikipedia.org/wiki/John_C._Dvorak">John</a> to choose from.</p>
        To use the tool, create an account and then choose "Submit Artwork." You'll
        then be asked to choose an overlay style and then upload your artwork and pitch. We provide a tool to easily crop
        your image and we'll automatically overlay the standard No Agenda logo and frame (<strike>blatantly stolen</strike>
        appropriated from Sir Randy Asher and Sir Paul T.)</p>
        <p>Your artwork will be added to the submissions gallery and, if accepted, will be added to the Accepted Artwork
        gallery for posterity. If you don't want to make artwork, you can browse the galleries when you are in-between
        listening to the show and punching people in the mouth.</p>
    </div>
</section>

@stop
