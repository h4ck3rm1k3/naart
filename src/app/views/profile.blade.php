@extends('layouts.master')

@section('title')
@parent
 :: {{ $title }}
@stop

@section('css')
@parent
<link rel="stylesheet" href="/assets/css/cropper.css">
@stop

@section('content')
<div class="container">
    <div class="row center">
        <h1>{{ $artist->profile->name }}</h1>
        <h3>Crafting Fine Album Art Since {{ $artist->created_at->format('l, F jS, Y') }}</h3>
        @if (!is_null($artist->profile->website))
        <h4>Check Out <a href="{{ $artist->profile->website }}">{{ $artist->profile->website }}</a></h4>
        @endif
        @if (!is_null($artist->profile->location))
        <h4>{{ $artist->profile->name }} currently is residing in {{ $artist->profile->location }}</h4>
        @endif

    </div>
    @if ($can_edit)
    <div class="row">
        <h4>Editing Profiles Coming VERY Soon!</h4>
    </div>
    @endif
    <div class="row fx">
        <h4 class="center">Art submitted by {{ $artist->profile->name }}</h4>
        @if (count($artworks) == 0)
            <p>{{ $artist->profile->name }} hasn't submitted any artwork yet. Bug them to get busy!</p>
        @else
        @foreach($artworks as $artwork)
            <div class="col-xs-6 col-md-3 img artworkwrapper {{{ isset($artwork->accepted_for->episode_number) ? 'selected' : '' }}}"
            @if (isset($artwork->accepted_for))
                 data-accepted_for="{{{ $artwork->accepted_for->episode_number + 0 }}}" 
            @endif
            >
            @if (isset($artwork->accepted_for))
                <div class="acceptedribbon"><span class="fa fa-star"></span> Ep. {{{ $artwork->accepted_for->episode_number + 0 }}} <span class="fa fa-star"></span></div>
            @endif
                <img
                 class="artwork" src="{{ $artwork->path }}/{{ $artwork->filehash }}_thumbs/{{ $artwork->filehash }}_320.png"
                 @if (isset($artwork->accepted_for))
                    title="{{ $artwork->title }} - Accepted for Episode {{{ $artwork->accepted_for->episode_number + 0 }}},
                    &ldquo;{{{ $artwork->accepted_for->title }}}&rdquo;"
                 @else
                    title="{{ $artwork->title }}"
                 @endif
                 >
                 <div class="fx-overlay">
                    <a href="/artwork/{{ $artwork->id }}" class="fx-expand"><span class="fa fa-search"></span></a>
                    <a class="close-fx-overlay hidden">x</a>
                    <div class="fx-info">
                        <h3>{{ $artwork->title }}</h3>
                        <h4>By {{{ $artwork->user->profile->name }}}</h4>
                        @if (isset($artwork->accepted_for))
                            <h4 class="acceptedlink"><a href="/episode/{{{ $artwork->accepted_for->id }}}">Album Art Selected for Episode {{{ $artwork->accepted_for->episode_number + 0 }}}</a></h4>
                        @endif
                    </div>
                 </div>
            </div>
        @endforeach
        @endif
    </div>
</div>
<div class="container center">
    {{ $artworks->links() }}
</div>
@stop

@if ($can_edit)
    @section('scripts')
    @parent
    <script src="/assets/js/dochange.js"></script>
    @stop
@endif
