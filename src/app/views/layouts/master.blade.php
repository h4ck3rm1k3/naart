<!DOCTYPE html>
<html lang="en">
    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        {{-- icons start --}}
        <link rel="apple-touch-icon-precomposed" sizes="57x57" href="/assets/icon/apple-touch-icon-57x57.png" />
        <link rel="apple-touch-icon-precomposed" sizes="114x114" href="/assets/icon/apple-touch-icon-114x114.png" />
        <link rel="apple-touch-icon-precomposed" sizes="72x72" href="/assets/icon/apple-touch-icon-72x72.png" />
        <link rel="apple-touch-icon-precomposed" sizes="144x144" href="/assets/icon/apple-touch-icon-144x144.png" />
        <link rel="apple-touch-icon-precomposed" sizes="60x60" href="/assets/icon/apple-touch-icon-60x60.png" />
        <link rel="apple-touch-icon-precomposed" sizes="120x120" href="/assets/icon/apple-touch-icon-120x120.png" />
        <link rel="apple-touch-icon-precomposed" sizes="76x76" href="/assets/icon/apple-touch-icon-76x76.png" />
        <link rel="apple-touch-icon-precomposed" sizes="152x152" href="/assets/icon/apple-touch-icon-152x152.png" />
        <link rel="icon" type="image/png" href="/assets/icon/favicon-196x196.png" sizes="196x196" />
        <link rel="icon" type="image/png" href="/assets/icon/favicon-96x96.png" sizes="96x96" />
        <link rel="icon" type="image/png" href="/assets/icon/favicon-32x32.png" sizes="32x32" />
        <link rel="icon" type="image/png" href="/assets/icon/favicon-16x16.png" sizes="16x16" />
        <link rel="icon" type="image/png" href="/assets/icon/favicon-128.png" sizes="128x128" />
        <meta name="application-name" content="No Agenda Art Generator"/>
        <meta name="msapplication-TileColor" content="#FFFFFF" />
        <meta name="msapplication-TileImage" content="/assets/icon/mstile-144x144.png" />
        <meta name="msapplication-square70x70logo" content="/assets/icon/mstile-70x70.png" />
        <meta name="msapplication-square150x150logo" content="/assets/icon/mstile-150x150.png" />
        <meta name="msapplication-wide310x150logo" content="/assets/icon/mstile-310x150.png" />
        <meta name="msapplication-square310x310logo" content="/assets/icon/mstile-310x310.png" />
        {{-- icons end --}}
        <title>@section('title')No Agenda Art Generator @show</title>
        @section('css')
        <link rel="stylesheet" href="/assets/css/app.min.css">
        <!--[if gte IE 9]>
          <style type="text/css">
            .gradient {
               filter: none;
            }
          </style>
        <![endif]-->
        @show
    </head>
    <body>
        @include('partials.navbar')
        @section('pageheading')
        @show
        @include('partials.notifications')
        @yield('content')
        @section('pagefooter')
        <footer id="pagefooter">
            <div class="container">
                <div class="col-sm-6 copyright">
                    <p>Artwork Overlays by Sir Randy Asher and Sir Paul T are used in the Generator. Unless otherwise noted,
                    this site and the work submitted are licensed under a Creative Commons License. Copyright &copy; 2010-{{ date('Y') }},
                    Paul Couture, Some Rights Reserved.</p>
                    <p>By submitting artwork, you are acknowledging you have the right to publish the work and are, by submitting the work,
                    agreeing to place it under the Creative Commons Attribution-Share Alike 3.0 United States License.</p>
                    <p class="center"><a rel="license" href="http://creativecommons.org/licenses/by-sa/3.0/us/"
                    ><img alt="Creative Commons License" style="border-width:0"
                    src="http://creativecommons.org/images/public/somerights20.png"></a></p>
                    <p class="center">No Agenda Album Art Generator by Paul Couture is licensed under a <a href="http://creativecommons.org/licenses/by-sa/3.0/us/"
                    >Creative Commons Attribution-Share Alike 3.0 United States License</a>.</p>
                </div>
                <div class="col-sm-3 links">
                    <ul class="list-unstyled">
                        <li><a href="http://www.noagendashow.com">No Agenda Show</a></li>
                        <li><a href="http://archive.noagendanotes.com">The Archive</a></li>
                        <li><a href="http://noagendastream.com">No Agenda Stream</a></li>
                        <li><a href="http://noagendachat.net">No Agenda Chat</a></li>
                        <li><a href="http://noagendanation.com">No Agenda Nation</a></li>
                    </ul>
                </div>
                <div class="col-sm-3 donate">
                <ul class="list-unstyled">
                    <li>Don't Be A Douchebag - Donate to the Show
                        <ul><li><a href="http://noagenda.squarespace.com/donations/">Donate Here</a></li>
                        <li><a href="http://dvorak.org/NA">Or Here</a></li></ul>
                    </li>
                    <li>Drop us a tip if you like the Generator
                        <div class="center">
                            <form action="https://www.paypal.com/cgi-bin/webscr" method="post" target="_top">
                            <input type="hidden" name="cmd" value="_donations">
                            <input type="hidden" name="business" value="bonked@noagendaartgenerator.com">
                            <input type="hidden" name="lc" value="US">
                            <input type="hidden" name="item_name" value="No Agenda Art Generator Tip Jar">
                            <input type="hidden" name="no_note" value="0">
                            <input type="hidden" name="currency_code" value="USD">
                            <input type="hidden" name="bn" value="PP-DonationsBF:btn_donateCC_LG.gif:NonHostedGuest">
                            <input type="image" src="https://www.paypalobjects.com/en_US/i/btn/btn_donateCC_LG.gif" border="0" name="submit" alt="PayPal - The safer, easier way to pay online!">
                            <img alt="" border="0" src="https://www.paypalobjects.com/en_US/i/scr/pixel.gif" width="1" height="1">
                            </form></div>
                    </li>
                </ul>
                </div>
            </div>
        </footer>
        @show
        @section('scripts')
        <script src="//cdnjs.cloudflare.com/ajax/libs/modernizr/2.8.3/modernizr.min.js"></script>
        <script src="/assets/js/app.min.js"></script>
        @show
    </body>
</html>
