@extends('layouts.master')

@section('title')@parent:: {{{ $title }}} @stop

@section('content')
<div class="container">
    <h1>Accepted Artwork</h1>
    <p>Below you will find the accepted artwork and episode details for the <a href="http://www.noagendashow.com">No Agenda Show</a>.</p>
    <div class="row">
    @foreach($episodes as $episode)

        <?php $artwork = $episode->albumart($episode->artwork_id); ?>
        <div class="col-xs-6 col-md-3 artworkwrapper">
            <a href="{{ url('episode/' . $episode->id) }}"
                class="gallerygrid"><img class="artwork"
            @if ($episode->artwork_id > 0)
                src="{{ $artwork->path }}/{{ $artwork->filehash }}_thumbs/{{ $artwork->filehash }}_320.png"
            @else
                src="/assets/img/artplaceholder320.jpg"
            @endif
                title="Episode {{ $episode->episode_number + 0 }} - &ldquo;{{ $episode->title }}&rdquo;"
                ></a>
        </div>
    @endforeach
    </div>
</div>
<div class="container center">
	{{ $episodes->links() }}
</div>
@stop
