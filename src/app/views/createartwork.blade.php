@extends('layouts.master')

@section('title')
@parent
 :: {{ $title }}
@stop

@section('content')
    <div class="container">
	    <div class="row center">
            <h1>Drag and Drop Your Artwork or Click Below to Start</h1>
        </div>
        <div class="row">
            @if (Session::get('error'))
                <div class="alert alert-error alert-danger">
                    @if (is_array(Session::get('error')))
                        {{ head(Session::get('error')) }}
                    @endif
                </div>
            @endif

            @if (Session::get('notice'))
                <div class="alert">{{ Session::get('notice') }}</div>
            @endif
            <div id="dropbox" class="center">
                <input type="hidden" id="_token" name="_token" value="{{{ csrf_token() }}}" />
                <h1 id="upinstruct"><span class="fa fa-cloud-upload fa-5x"></span><br>Drop your image here to upload</h1>
                <input type="file" id="artworkfile" style="display:none">
            </div>
            <div class="preview"></div>
            <div class="img-container">
        </div>
            <div class="img-preview"></div>
            <form id="gostep2" role="form" method="post" action="/crop">
                <input type="hidden" id="newimagepath" name="newimagepath" value="">
                <input type="hidden" id="newimagename" name="newimagename" value="">
                <input type="hidden" name="_token" value="{{{ csrf_token() }}}">
            <form>
        </div>
	</div>
@stop

@section('scripts')
@parent
<script src="/assets/js/docreate.js"></script>
@stop
