@extends('layouts.master')

@section('title')
@parent
 :: {{ $title }}
@stop

@section('css')

@section('content')
<div class="container">
    <div class="row center">
        <h1 class="artworktitle">Episode {{ $episode->episode_number + 0 }} &quot;{{ $episode->title }}&quot;<br><span class="headlinedate">{{ $showdate }}</span></h1>
        <h2>Art Director:
            @if (!is_null($approved_art))
                <a href="/artist/{{ $approved_art->user->id }}">{{{ $approved_art->user->profile->name }}}</a>
            @else
                TBD
            @endif
        </h2>
    </div>
    <div class="row">
        <div class="col-md-6 acceptedartwork center">
            @if (!is_null($approved_art))
                <img src="{{ $approved_art->path }}/{{ $approved_art->filename }}">
            @else
                <img src="/assets/img/artplaceholder512.jpg">
            @endif
        </div>
        <div class="col-md-6">
            <ul class="list-unstyled">
                <li><a href="{{ $episode->link }}">Direct Link to Episode</a></li>
            </ul>
            @if ($submitted_art->count() > 0)
            <div class="row alsoran">
                <h4 class="center">Submitted Artwork for this Episode</h4>
                @foreach ($submitted_art as $art)
                    <div class="col-sm-2 artworkwrapper">
                        <a class="artthumblink" href="/artwork/{{ $art->id }}"
                        @if ($can_edit)
                        data-populateepisode="{{ $art->id }}"
                        @endif
                        title="{{ $art->title }} by {{ $art->user->profile->name }}"
                        ><img src="{{ $art->path }}/{{ $art->filehash }}_thumbs/{{ $art->filehash }}_72.png"></a>
                    </div>
                @endforeach
            </div>
            <div class="row center">
                {{ $submitted_art->links() }}
                @if ($can_edit)
                </div>
                <div class="row center">
                <div class="checkbox">
                    <label>
                        <input type="checkbox" id="populateToggle" checked> Poplate Form instead of following Links?
                    </label>
                </div>
                @endif
            </div>

            @endif
        </div>
    </div>
    @if ($can_edit)
        <div class="row">
            <h3>Manage Episode</h3>
            <form role="form" class="form-horizontal" method="post" action="/episode/update">
                <input type="hidden" name="_token" value="{{ Session::getToken() }}">
                <input type="hidden" name="episodeid" value="{{ $episode->id }}">
                <div class="form-group">
                    <label for="acceptedartwork" class="col-sm-2 control-label">Approved Artwork</label>
                    <div class="col-sm-4">
                        <input class="form-control" type="numeric" name="acceptedartwork"
                        id="acceptedartwork"
                        placeholder="The Artwork ID"
                        value="{{{ Input::old('acceptedartwork', isset($post) ? $post->acceptedartwork : $episode->artwork_id) }}}">
                    </div>
                </div>
                <div class="form-group">
                    <label for="showtitle" class="col-sm-2 control-label">Title</label>
                    <div class="col-sm-4">
                        <input class="form-control" type="text" name="showtitle"
                        placeholder="Show Title"
                        value="{{{ Input::old('showtitle', isset($post) ? $post->showtitle : $episode->title) }}}">
                    </div>
                </div>
                <div class="form-group">
                    <label for="showdate" class="col-sm-2 control-label">Date</label>
                    <div class="col-sm-4">
                        <input class="form-control" type="text" id="showdate" name="showdate"
                        data-date-format="YYYY-MM-DD"
                        placeholder="Show Date"
                        value="{{{ Input::old('showtitle', isset($post) ? $post->showdate : $episode->show_date) }}}">
                    </div>
                </div>
                <div class="form-group">
                    <label for="episodenumber" class="col-sm-2 control-label">Episode Number</label>
                    <div class="col-sm-4">
                        <input class="form-control" type="text" name="episodenumber"
                        placeholder="Episode Number"
                        value="{{{ Input::old('episodenumber', isset($post) ? $post->episodenumber : $episode->episode_number) }}}">
                    </div>
                </div>
                <div class="form-group">
                    <label for="showlink" class="col-sm-2 control-label">MP3 Link</label>
                    <div class="col-sm-4">
                        <input class="form-control" type="text" name="showlink"
                        placeholder="Full MP3 Link to Show"
                        value="{{{ Input::old('showlink', isset($post) ? $post->showlink : $episode->link) }}}">
                    </div>
                </div>
                <div class="form-group">
                    <label for="published" class="col-sm-2 control-label">Published?</label>
                    <div class="col-sm-4">
                        <select class="form-control" name="published">
                            <option value="1"
                            @if ($episode->published)
                            selected
                            @endif
                            >Yes</option>
                            <option value="0"
                            @if (!$episode->published)
                            selected
                            @endif
                            >No</option>
                        </select>
                    </div>
                </div>
                <div class="col-sm-offset-2">
                    <button type="submit" class="btn btn-primary">Assign Artwork</button>
                </div>
            </form>
        </div>
    @endif
</div>
@stop

@if ($can_edit)
    @section('scripts')
    @parent
    @if ($can_edit)
    <script src="/assets/js/populateepisode.js"></script>
    @endif
    @stop
@endif
